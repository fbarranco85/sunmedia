let counter = 0;
let counterFailed = 1;
const NETWORK_ERROR_PROBABILITY = 0.1;

function fakeFetch(ad, cb) {
  const fakeResponses = [
    { ad: 1, title: 'The first ad' },
    { ad: 2, title: 'The second ad' },
    { ad: 3, title: 'The third ad' },
    { ad: 4, title: 'The forth ad' },
    { ad: 5, title: 'The last ad' },
  ];
  const randomDelay = (Math.round(Math.random() * 1E4) % 40) + 1000;

  setTimeout(() => {
    const networkError = Math.random() <= NETWORK_ERROR_PROBABILITY;
    const currentAd = fakeResponses.find(resp => resp.ad === ad);

    if (networkError) {
      cb('Network error');
    } else {
      cb(null, currentAd);
    }
  }, randomDelay);
}

function * generator(callback) {
  for(let i = 1; i < 6; i += 1) {
    counter = i;
    yield (fakeFetch(i, callback));
  }
  console.log('Done!');
}

const genFail = (counter) => {
  new Promise((resolve, reject) => {
    const callbackFailed = (error, success) => {
      if(error) {
        console.log(`Failed to load the ad ${counter}`);
        if(counterFailed < 2) {
          counterFailed += 1;
          genFail(counter);
        }
        return;
      }
      counterFailed = 1;
      resolve(success);
    }
    fakeFetch(counter, callbackFailed);
  }).then((response) => {
    console.log(response.title)
    iterator.next();
  });
}

const iterator = generator((error, success) => {
  if(error) {
    console.log(`Failed to load the ad ${counter}`);
    genFail(counter)
    return;
  }
  console.log(success.title);
  iterator.next();
});

iterator.next()