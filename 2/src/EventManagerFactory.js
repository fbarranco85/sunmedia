import EventManager from './EventManager';
import Event from './Event';

export default class EventManagerFactory{
    static create(events, types) {
        let second = 0;
        let interval = true;
        const length = Event.getLength(events);
        const process = () => {
            const enableEvents = Event.getTypeEvent(events, types);
            const getEvent = Event.getTimeEvent(enableEvents, second);
            Event.printEvent(getEvent, second);
            while (second === events[length - 1].second) {
                interval = false;
                break;
            }
            second += 1;
        };
        setInterval(() => process(), 1000);
        return new EventManager();
    }
};