export default class Event{
// implement your code here...
    static getLength(events) {
        return events.length;
    }

    static getTypeEvent(events, types) {
        return events.filter(event => types.includes(event.type));
    }

    static getTimeEvent (events, second) {
        return events.filter((event) => {
            return event.second === second
        });
    }

    static printEvent(events, second) {
        return events.map((event) => {
            console.log(`At second ${second}: {type: "${event.type}", message: "${event.message}"}`)
        })
    }
};